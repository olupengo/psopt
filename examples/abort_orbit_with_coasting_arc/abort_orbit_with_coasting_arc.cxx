//////////////////////////////////////////////////////////////////////////
//////////////////         abort_orbit.cxx          //////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////           PSOPT  Example             ////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////// Title:     Abort orbit with coasting arc         ////////////////
//////// Last modified: 18 December 2020                  ////////////////
//////// Reference:     PSOPT Manual                      ////////////////
//////// (See PSOPT handbook for full reference)          ////////////////
//////////////////////////////////////////////////////////////////////////
////////            Copyright (c) Peng Lu, 2020           ////////////////
//////////////////////////////////////////////////////////////////////////

#include "psopt.h"

using namespace PSOPT;


//////////////////////////////////////////////////////////////////////////
///////////////////  Declare auxiliary functions  ////////////////////////
//////////////////////////////////////////////////////////////////////////

void oe2rv(MatrixXd &oe, double mu, MatrixXd *ri, MatrixXd *vi);

void rv2oe(adouble *rv, adouble *vv, double mu, adouble *oe);


//////////////////////////////////////////////////////////////////////////
/////////  Declare an auxiliary structure to hold local constants  ///////
//////////////////////////////////////////////////////////////////////////

struct Constants {
    MatrixXd *omega_matrix;
    double mu;
    double cd;
    double sa;
    double rho0;
    double H;
    double Re;
    double g0;
    double thrust_srb;
    double thrust_first;
    double thrust_second;
    double ISP_srb;
    double ISP_first;
    double ISP_second;
    double h_ast[3];  // specified angular momentum of the abort orbit
    int circular_abort;  // flag for circular abort orbit optimization
    double rp;  // geocentric distance of the perigee of the nominal orbit
};

typedef struct Constants Constants_;

//////////////////////////////////////////////////////////////////////////
///////////////////  Define the end point (Mayer) cost function //////////
//////////////////////////////////////////////////////////////////////////

adouble endpoint_cost(adouble *initial_states, adouble *final_states,
                      adouble *parameters, adouble &t0, adouble &tf,
                      adouble *xad, int iphase, Workspace *workspace) {

    Constants_ &CONSTANTS = *((Constants_ *) workspace->problem->user_data);

    adouble retval;
    adouble rf[3], vf[3];
    rf[0] = final_states[0];
    rf[1] = final_states[1];
    rf[2] = final_states[2];
    vf[0] = final_states[3];
    vf[1] = final_states[4];
    vf[2] = final_states[5];

    if (iphase < 3)
        retval = 0.0;

    if (iphase == 3) {
        if (CONSTANTS.circular_abort) {
            // For circular abort orbit, maximize the orbital altitude
            retval = -sqrt(dot(rf, rf, 3));
        } else {
            // For elliptical abort orbit, maximize the orbital energy
            retval = -sqrt(dot(vf, vf, 3));
        }
    }

    return retval;
}


//////////////////////////////////////////////////////////////////////////
///////////////////  Define the integrand (Lagrange) cost function  //////
//////////////////////////////////////////////////////////////////////////

adouble integrand_cost(adouble *states, adouble *controls,
                       adouble *parameters, adouble &time,
                       adouble *xad, int iphase, Workspace *workspace) {
    return 0.0;
}



//////////////////////////////////////////////////////////////////////////
///////////////////  Define the DAE's ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void dae(adouble *derivatives, adouble *path, adouble *states,
         adouble *controls, adouble *parameters, adouble &time,
         adouble *xad, int iphase, Workspace *workspace) {


    Constants_ &CONSTANTS = *((Constants_ *) workspace->problem->user_data);

//    adouble *x = states;
//    adouble *u = controls;

    adouble r[3];  // position
    r[0] = states[0];
    r[1] = states[1];
    r[2] = states[2];

    adouble v[3];  // velocity
    v[0] = states[3];
    v[1] = states[4];
    v[2] = states[5];

    adouble m = states[6];

    adouble u[3];  // thrust direction unit vector
    u[0] = controls[0];
    u[1] = controls[1];
    u[2] = controls[2];

    adouble T = controls[3];  // thrust magnitude


    int j;
    adouble thrust[3], grav[3];  // acceleration
    adouble rdot[3], vdot[3], mdot;  // derivatives

    // thrust acceleration vector
    for (j = 0; j < 3; j++) { thrust[j] = (T / m) * u[j]; }

    // gravity vector
    adouble rad = sqrt(dot(r, r, 3));
    adouble muoverradcubed = (CONSTANTS.mu) / (pow(rad, 3));
    for (j = 0; j < 3; j++) { grav[j] = -muoverradcubed * r[j]; }

    for (j = 0; j < 3; j++) { rdot[j] = v[j]; }
    for (j = 0; j < 3; j++) { vdot[j] = thrust[j] + grav[j]; }
    mdot = -T / (CONSTANTS.g0 * CONSTANTS.ISP_second);


    derivatives[0] = rdot[0];
    derivatives[1] = rdot[1];
    derivatives[2] = rdot[2];
    derivatives[3] = vdot[0];
    derivatives[4] = vdot[1];
    derivatives[5] = vdot[2];
    derivatives[6] = mdot;

    path[0] = dot(u, u, 3);  // thrust direction unit vector

}

////////////////////////////////////////////////////////////////////////////
///////////////////  Define the events function ////////////////////////////
////////////////////////////////////////////////////////////////////////////

void events(adouble *e, adouble *initial_states, adouble *final_states,
            adouble *parameters, adouble &t0, adouble &tf, adouble *xad,
            int iphase, Workspace *workspace) {


    Constants_ &CONSTANTS = *((Constants_ *) workspace->problem->user_data);
    adouble h_ast[3];
    h_ast[0] = CONSTANTS.h_ast[0];
    h_ast[1] = CONSTANTS.h_ast[1];
    h_ast[2] = CONSTANTS.h_ast[2];

    adouble rf[3];
    rf[0] = final_states[0];
    rf[1] = final_states[1];
    rf[2] = final_states[2];
    adouble vf[3];
    vf[0] = final_states[3];
    vf[1] = final_states[4];
    vf[2] = final_states[5];

    if (iphase == 1) {
        // These events are related to the initial state conditions
        for (int j = 0; j < 7; j++) e[j] = initial_states[j];
    }

    if (iphase == 3) {
        e[0] = dot(rf, h_ast, 3);  // coplanar constraints
        e[1] = dot(vf, h_ast, 3);
        e[2] = dot(rf, vf, 3);  // perpendicular constraint
        if (CONSTANTS.circular_abort) {  // for circular abort orbit
            e[3] = sqrt(dot(rf, rf, 3)) * dot(vf, vf, 3);  //
        } else {  // for elliptical abort orbit
            e[3] = sqrt(dot(rf, rf, 3));  // perigee altitude constraint
        }
    }

}


///////////////////////////////////////////////////////////////////////////
///////////////////  Define the phase linkages function ///////////////////
///////////////////////////////////////////////////////////////////////////

void linkages(adouble *linkages, adouble *xad, Workspace *workspace) {

    int index = 0;

    auto_link(linkages, &index, xad, 1, 2, workspace);
    auto_link(linkages, &index, xad, 2, 3, workspace);
}



////////////////////////////////////////////////////////////////////////////
///////////////////  Define the main routine ///////////////////////////////
////////////////////////////////////////////////////////////////////////////

int main(void) {
////////////////////////////////////////////////////////////////////////////
///////////////////  Declare key structures ////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    Alg algorithm;
    Sol solution;
    Prob problem;

////////////////////////////////////////////////////////////////////////////
///////////////////  Register problem name  ////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    problem.name = "Abort orbit";
    problem.outfilename = "abort_orbit.txt";

////////////////////////////////////////////////////////////////////////////
///////////////////  Declare an instance of Constants structure /////////////
////////////////////////////////////////////////////////////////////////////

    Constants_ CONSTANTS;

    problem.user_data = (void *) &CONSTANTS;

////////////////////////////////////////////////////////////////////////////
////////////  Define problem level constants & do level 1 setup ////////////
////////////////////////////////////////////////////////////////////////////

    problem.nphases = 3;
    problem.nlinkages = 16;


    psopt_level1_setup(problem);

/////////////////////////////////////////////////////////////////////////////
/////////   Define phase related information & do level 2 setup  ////////////
/////////////////////////////////////////////////////////////////////////////

    problem.phases(1).nstates = 7;  // [rx,ry,rz,vx,vy,vx,m]
    problem.phases(1).ncontrols = 4;  // [ux,uy,uz,T]
    problem.phases(1).nevents = 7;
    problem.phases(1).npath = 1;

    problem.phases(2).nstates = 7;
    problem.phases(2).ncontrols = 4;
    problem.phases(2).nevents = 0;
    problem.phases(2).npath = 1;

    problem.phases(3).nstates = 7;
    problem.phases(3).ncontrols = 4;
    problem.phases(3).nevents = 4;
    problem.phases(3).npath = 1;

    problem.phases(1).nodes << 20;
    problem.phases(2).nodes << 20;
    problem.phases(3).nodes << 10;


    psopt_level2_setup(problem, algorithm);

////////////////////////////////////////////////////////////////////////////
///////////////////  Initialize CONSTANTS and //////////////////////////////
///////////////////  declare local variables  //////////////////////////////
////////////////////////////////////////////////////////////////////////////


    CONSTANTS.mu = 3.9860044e14;       // Gravitational parameter (m^3/s^2)
    CONSTANTS.Re = 6371393.0;         // Radius of earth (m)
    CONSTANTS.g0 = 9.81904;           // sea level gravity (m/s^2)

    CONSTANTS.circular_abort = 0;  // 1优化圆终止轨道，0优化椭圆终止轨道
    double t0 = 0;      // the time of fault
    double tf;    // flight time

    MatrixXd r0(3, 1), v0(3, 1);
    double m0, m_dry, Thrust, Isp, loss, af, ef, incf, Omf, omf, nu_guess;

    loss = 0.2;
    int which_case = 1;  // 不同算例
    switch (which_case) {
        case 0:  // LEO任务——长征七号空间站空间站转移轨道发射任务
            r0 << -5917.782e3, -1325.058e3, 2365.570e3;  // initial position (m)
            v0 << 756, -2852, 2169;  // velocity (m/s)

            m0 = 99180;     // wet mass at the time of fault (kg)
            m_dry = 25000;  // dry mass including payload (kg)
            Thrust = (1 - loss) * 720e3;      // vacuum thrust of the second stage (N)
            Isp = 342;      // specific impulse (s)
            tf = 800;

            // LEO example (China Space Station Transfer Orbit)
            af = 6681.393e3;
            ef = 0.01347;
            incf = 41.50 * pi / 180.0;
            Omf = 165.34 * pi / 180.0;
            omf = 49.60 * pi / 180.0;
            nu_guess = 0;
            break;
        case 1:  // GTO任务
            r0 << 5477.105e3, 2000.179e3, 2930.152e3;  // initial position (m)
            v0 << -1832, 6068, -966;             // velocity (m/s)

            m0 = 17721;     // wet mass at the time of fault (kg)
            m_dry = 6644;  // dry mass including payload (kg)
            Thrust = (1 - loss) * 110094;    // vacuum thrust of the second stage (N)
            Isp = 467.2;      // specific impulse (s)
            tf = 1469;

            // GTO
            af = 24392827;
            ef = 0.72855;
            incf = 20.00 * pi / 180.0;
            Omf = 269.80 * pi / 180.0;
            omf = 211.40 * pi / 180.0;
            nu_guess = 0;
            break;
    }

    MatrixXd oe(6, 1);
    oe << af, ef, incf, Omf, omf, nu_guess;

    MatrixXd rout(3, 1);
    MatrixXd vout(3, 1);
    oe2rv(oe, CONSTANTS.mu, &rout, &vout);

    rout = rout.transpose().eval();
    vout = vout.transpose().eval();

    CONSTANTS.thrust_second = Thrust;
    CONSTANTS.ISP_second = Isp;
    CONSTANTS.h_ast[0] = sin(incf) * sin(Omf);
    CONSTANTS.h_ast[1] = -sin(incf) * cos(Omf);
    CONSTANTS.h_ast[2] = cos(incf);
    CONSTANTS.rp = af * (1 - ef);

    double rmin = -2 * CONSTANTS.Re;
    double rmax = -rmin;
    double vmin = -10000.0;
    double vmax = -vmin;


////////////////////////////////////////////////////////////////////////////
///////////////////  Enter problem bounds information //////////////////////
////////////////////////////////////////////////////////////////////////////

    // Phase 1 bounds
    int iphase = 1;

    problem.phases(iphase).bounds.lower.states << rmin, rmin, rmin, vmin,
            vmin, vmin, m_dry;
    problem.phases(iphase).bounds.upper.states << rmax, rmax, rmax, vmax,
            vmax, vmax, m0;

    problem.phases(iphase).bounds.lower.controls << -1.0, -1.0, -1.0, Thrust;
    problem.phases(iphase).bounds.upper.controls << 1.0, 1.0, 1.0, Thrust;

    problem.phases(iphase).bounds.lower.path << 1.0;
    problem.phases(iphase).bounds.upper.path << 1.0;


    // The following bounds fix the initial state conditions.

    problem.phases(iphase).bounds.lower.events << r0(0), r0(1),
            r0(2), v0(0), v0(1), v0(2), m0;
    problem.phases(iphase).bounds.upper.events << r0(0), r0(1),
            r0(2), v0(0), v0(1), v0(2), m0;

    problem.phases(iphase).bounds.lower.StartTime = t0;
    problem.phases(iphase).bounds.upper.StartTime = t0;

    problem.phases(iphase).bounds.lower.EndTime = t0;
    problem.phases(iphase).bounds.upper.EndTime = tf;


    // Phase 2 bounds
    iphase = 2;

    problem.phases(iphase).bounds.lower.states << rmin, rmin, rmin, vmin, vmin, vmin, m_dry;
    problem.phases(iphase).bounds.upper.states << rmax, rmax, rmax, vmax, vmax, vmax, m0;


    problem.phases(iphase).bounds.lower.controls << -1.0, -1.0, -1.0, 0.0;
    problem.phases(iphase).bounds.upper.controls << 1.0, 1.0, 1.0, 0.0;

    problem.phases(iphase).bounds.lower.path << 1.0;
    problem.phases(iphase).bounds.upper.path << 1.0;

    problem.phases(iphase).bounds.lower.StartTime = t0;
    problem.phases(iphase).bounds.upper.StartTime = tf;

    problem.phases(iphase).bounds.lower.EndTime = t0;
    problem.phases(iphase).bounds.upper.EndTime = tf;


    // Phase 2 bounds
    iphase = 3;

    problem.phases(iphase).bounds.lower.states << rmin, rmin, rmin, vmin, vmin, vmin, m_dry;
    problem.phases(iphase).bounds.upper.states << rmax, rmax, rmax, vmax, vmax, vmax, m0;

    problem.phases(iphase).bounds.lower.controls << -1.0, -1.0, -1.0, Thrust;
    problem.phases(iphase).bounds.upper.controls << 1.0, 1.0, 1.0, Thrust;

    problem.phases(iphase).bounds.lower.path << 1.0;
    problem.phases(iphase).bounds.upper.path << 1.0;

    if (CONSTANTS.circular_abort) {
        problem.phases(iphase).bounds.lower.events << 0, 0, 0, CONSTANTS.mu;
        problem.phases(iphase).bounds.upper.events << 0, 0, 0, CONSTANTS.mu;
    } else {
        problem.phases(iphase).bounds.lower.events << 0, 0, 0, CONSTANTS.rp;
        problem.phases(iphase).bounds.upper.events << 0, 0, 0, CONSTANTS.rp;
    }


    problem.phases(iphase).bounds.lower.StartTime = t0;
    problem.phases(iphase).bounds.upper.StartTime = tf;

    problem.phases(iphase).bounds.lower.EndTime = tf;
    problem.phases(iphase).bounds.upper.EndTime = tf;

////////////////////////////////////////////////////////////////////////////
///////////////////  Define & register initial guess ///////////////////////
////////////////////////////////////////////////////////////////////////////

    iphase = 1;

    problem.phases(iphase).guess.states = zeros(7, 5);

    problem.phases(iphase).guess.states.row(0) = linspace(r0(0), rout(0), 5);
    problem.phases(iphase).guess.states.row(1) = linspace(r0(1), rout(1), 5);
    problem.phases(iphase).guess.states.row(2) = linspace(r0(2), rout(2), 5);
    problem.phases(iphase).guess.states.row(3) = linspace(v0(0), vout(0), 5);
    problem.phases(iphase).guess.states.row(4) = linspace(v0(1), vout(1), 5);
    problem.phases(iphase).guess.states.row(5) = linspace(v0(2), vout(2), 5);
    problem.phases(iphase).guess.states.row(6) = linspace(m0, m_dry, 5);

    problem.phases(iphase).guess.controls = zeros(4, 5);

    problem.phases(iphase).guess.controls.row(0) = ones(1, 5);
    problem.phases(iphase).guess.controls.row(1) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(2) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(3) = ones(1, 5) * Thrust;

    problem.phases(iphase).guess.time = linspace(t0, tf / 2, 5);


    iphase = 2;

    problem.phases(iphase).guess.states = zeros(7, 5);

    problem.phases(iphase).guess.states.row(0) = linspace(r0(0), rout(0), 5);
    problem.phases(iphase).guess.states.row(1) = linspace(r0(1), rout(1), 5);
    problem.phases(iphase).guess.states.row(2) = linspace(r0(2), rout(2), 5);
    problem.phases(iphase).guess.states.row(3) = linspace(v0(0), vout(0), 5);
    problem.phases(iphase).guess.states.row(4) = linspace(v0(1), vout(1), 5);
    problem.phases(iphase).guess.states.row(5) = linspace(v0(2), vout(2), 5);
    problem.phases(iphase).guess.states.row(6) = linspace(m0, m_dry, 5);

    problem.phases(iphase).guess.controls = zeros(4, 5);

    problem.phases(iphase).guess.controls.row(0) = ones(1, 5);
    problem.phases(iphase).guess.controls.row(1) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(2) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(3) = ones(1, 5) * Thrust;

    problem.phases(iphase).guess.time = linspace(tf / 2, tf, 5);


    iphase = 3;

    problem.phases(iphase).guess.states = zeros(7, 5);

    problem.phases(iphase).guess.states.row(0) = linspace(rout(0), rout(0), 5);
    problem.phases(iphase).guess.states.row(1) = linspace(rout(1), rout(1), 5);
    problem.phases(iphase).guess.states.row(2) = linspace(rout(2), rout(2), 5);
    problem.phases(iphase).guess.states.row(3) = linspace(vout(0), vout(0), 5);
    problem.phases(iphase).guess.states.row(4) = linspace(vout(1), vout(1), 5);
    problem.phases(iphase).guess.states.row(5) = linspace(vout(2), vout(2), 5);
    problem.phases(iphase).guess.states.row(6) = linspace(m_dry, m_dry, 5);

    problem.phases(iphase).guess.controls = zeros(4, 5);

    problem.phases(iphase).guess.controls.row(0) = ones(1, 5);
    problem.phases(iphase).guess.controls.row(1) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(2) = zeros(1, 5);
    problem.phases(iphase).guess.controls.row(3) = ones(1, 5) * Thrust;

    problem.phases(iphase).guess.time = linspace(3 * tf / 4, tf, 5);

////////////////////////////////////////////////////////////////////////////
///////////////////  Register problem functions  ///////////////////////////
////////////////////////////////////////////////////////////////////////////


    problem.integrand_cost = &integrand_cost;
    problem.endpoint_cost = &endpoint_cost;
    problem.dae = &dae;
    problem.events = &events;
    problem.linkages = &linkages;

////////////////////////////////////////////////////////////////////////////
///////////////////  Enter algorithm options  //////////////////////////////
////////////////////////////////////////////////////////////////////////////


    algorithm.nlp_method = "IPOPT";
//    algorithm.nlp_method = "SNOPT";
    algorithm.scaling = "automatic";
    algorithm.derivatives = "automatic";
    algorithm.nlp_iter_max = 1000;
    algorithm.collocation_method = "Chebyshev";
//    algorithm.collocation_method = "Legendre";
//    algorithm.mesh_refinement = "automatic";
    algorithm.ode_tolerance = 1.e-6;

////////////////////////////////////////////////////////////////////////////
///////////////////  Now call PSOPT to solve the problem   //////////////////
////////////////////////////////////////////////////////////////////////////

    psopt(solution, problem, algorithm);

////////////////////////////////////////////////////////////////////////////
///////////  Extract relevant variables from solution structure   //////////
////////////////////////////////////////////////////////////////////////////

    MatrixXd x_ph1, x_ph2, x_ph3, u_ph1, u_ph2, u_ph3, t_ph1, t_ph2, t_ph3;

    x_ph1 = solution.get_states_in_phase(1);
    x_ph2 = solution.get_states_in_phase(2);
    x_ph3 = solution.get_states_in_phase(3);

    u_ph1 = solution.get_controls_in_phase(1);
    u_ph2 = solution.get_controls_in_phase(2);
    u_ph3 = solution.get_controls_in_phase(3);

    t_ph1 = solution.get_time_in_phase(1);
    t_ph2 = solution.get_time_in_phase(2);
    t_ph3 = solution.get_time_in_phase(3);

////////////////////////////////////////////////////////////////////////////
///////////////////  Declare MatrixXd objects to store results //////////////
////////////////////////////////////////////////////////////////////////////

    MatrixXd x, u, T, t, t_, x_, u_, T_;

    if (1) {  // use gnuplot directly

        t_.resize(t_ph1.cols() + t_ph2.cols() + t_ph3.cols(), 1);  // t_
        x_.resize(x_ph1.cols() + x_ph2.cols() + x_ph3.cols(), 7);  // x_
        u_.resize(u_ph1.cols() + u_ph2.cols() + u_ph3.cols(), 3);  // u_
        T_.resize(u_ph1.cols() + u_ph2.cols() + u_ph3.cols(), 1);  // T_

        t_ << t_ph1.transpose().eval(), t_ph2.transpose().eval(), t_ph3.transpose().eval();
        x_ << x_ph1.transpose().eval(), x_ph2.transpose().eval(), x_ph3.transpose().eval();
        u_ << u_ph1.block(0, 0, 3, u_ph1.cols()).transpose().eval(),
                u_ph2.block(0, 0, 3, u_ph2.cols()).transpose().eval(),
                u_ph3.block(0, 0, 3, u_ph3.cols()).transpose().eval();
        T_ << u_ph1.block(3, 0, 1, u_ph1.cols()).transpose().eval(),
                u_ph2.block(3, 0, 1, u_ph2.cols()).transpose().eval(),
                u_ph3.block(3, 0, 1, u_ph3.cols()).transpose().eval();

        t = t_; // t
        x.resize(x_.rows(), 1 + 7);  // t_ x_
        u.resize(u_.rows(), 1 + 3);  // t_ u_
        T.resize(T_.rows(), 1 + 1);  // t_ T_

        x << t, x_;
        u << t, u_;
        T << t, T_;

////////////////////////////////////////////////////////////////////////////
///////////  Save solution data to files if desired ////////////////////////
////////////////////////////////////////////////////////////////////////////

        Save(x, "x.dat");
        Save(u, "u.dat");
        Save(T, "T.dat");
        Save(t, "t.dat");

    } else {  // use plot() in psopt

        x.resize(7, x_ph1.cols() + x_ph2.cols() + x_ph3.cols());
        u.resize(3, u_ph1.cols() + u_ph2.cols() + u_ph3.cols());
        T.resize(1, u_ph1.cols() + u_ph2.cols() + u_ph3.cols());
        t.resize(1, t_ph1.cols() + t_ph2.cols() + t_ph3.cols());

        x << x_ph1, x_ph2, x_ph3;
        u << u_ph1.block(0, 0, 3, u_ph1.cols()),
                u_ph2.block(0, 0, 3, u_ph2.cols()),
                u_ph3.block(0, 0, 3, u_ph3.cols());
        T << u_ph1.block(3, 0, 1, u_ph1.cols()),
                u_ph2.block(3, 0, 1, u_ph2.cols()),
                u_ph3.block(3, 0, 1, u_ph3.cols());
        t << t_ph1, t_ph2, t_ph3;

////////////////////////////////////////////////////////////////////////////
///////////  Save solution data to files if desired ////////////////////////
////////////////////////////////////////////////////////////////////////////

        Save(x, "x.dat");
        Save(u, "u.dat");
        Save(T, "T.dat");
        Save(t, "t.dat");

////////////////////////////////////////////////////////////////////////////
///////////  Plot some results if desired (requires gnuplot) ///////////////
////////////////////////////////////////////////////////////////////////////

        MatrixXd r, v, altitude, speed;

        r = x.block(0, 0, 3, x.cols());

        v = x.block(3, 0, 3, x.cols());

        altitude = (sum_columns(elemProduct(r, r)).cwiseSqrt()) / 1000.0;
        altitude = altitude - ones(1, altitude.cols()) * CONSTANTS.Re / 1000.0;  // altitude above the sea level (km)

        speed = sum_columns(elemProduct(v, v)).cwiseSqrt() / 1000.0;  // speed (km/s)

        T = T / 1000.0;

        plot(t, altitude, problem.name, "time (s)", "altitude (km)");

        plot(t, speed, problem.name, "time (s)", "speed (m/s)");

        plot(t, u, problem.name, "time (s)", "u");

        plot(t, T, problem.name, "time (s)", "thrust (kN)");


        plot(t, altitude, problem.name, "time (s)", "altitude (km)", "alt",
             "pdf", "abort_altitude.pdf");

        plot(t, speed, problem.name, "time (s)", "speed (m/s)", "speed",
             "pdf", "abort_speed.pdf");

        plot(t, u, problem.name, "time (s)", "u (dimensionless)", "u1 u2 u3",
             "pdf", "abort_thrust_direction.pdf");

        plot(t, T, problem.name, "time (s)", "thrust (kN)", "thrust",
             "pdf", "abort_thrust.pdf");
    }

}



////////////////////////////////////////////////////////////////////////////
////////////////// Define auxiliary functions //////////////////////////////
////////////////////////////////////////////////////////////////////////////


void rv2oe(adouble *rv, adouble *vv, double mu, adouble *oe) {
    int j;

    adouble K[3];
    K[0] = 0.0;
    K[1] = 0.0;
    K[2] = 1.0;

    adouble hv[3];
    cross(rv, vv, hv);

    adouble nv[3];
    cross(K, hv, nv);

    adouble n = sqrt(dot(nv, nv, 3));

    adouble h2 = dot(hv, hv, 3);

    adouble v2 = dot(vv, vv, 3);

    adouble r = sqrt(dot(rv, rv, 3));

    adouble ev[3];
    for (j = 0; j < 3; j++) ev[j] = 1 / mu * ((v2 - mu / r) * rv[j] - dot(rv, vv, 3) * vv[j]);

    adouble p = h2 / mu;

    adouble e = sqrt(dot(ev, ev, 3));        // eccentricity
    adouble a = p / (1 - e * e);                // semimajor axis
    adouble i = acos(hv[2] / sqrt(h2));        // inclination



#define USE_SMOOTH_HEAVISIDE
    double a_eps = 0.1;

#ifndef USE_SMOOTH_HEAVISIDE
    adouble Om = acos(nv[0]/n);			// RAAN
    if ( nv[1] < -PSOPT_extras::GetEPS() ){		// fix quadrant
        Om = 2*pi-Om;
    }
#endif

#ifdef USE_SMOOTH_HEAVISIDE

    adouble Om = smooth_heaviside((nv[1] + PSOPT_extras::GetEPS()), a_eps) * acos(nv[0] / n)
                 + smooth_heaviside(-(nv[1] + PSOPT_extras::GetEPS()), a_eps) * (2 * pi - acos(nv[0] / n));
#endif

#ifndef USE_SMOOTH_HEAVISIDE
    adouble om = acos(dot(nv,ev,3)/n/e);		// arg of periapsis
    if ( ev[2] < 0 ) {				// fix quadrant
        om = 2*pi-om;
    }
#endif

#ifdef USE_SMOOTH_HEAVISIDE
    adouble om = smooth_heaviside((ev[2]), a_eps) * acos(dot(nv, ev, 3) / n / e)
                 + smooth_heaviside(-(ev[2]), a_eps) * (2 * pi - acos(dot(nv, ev, 3) / n / e));
#endif

#ifndef USE_SMOOTH_HEAVISIDE
    adouble nu = acos(dot(ev,rv,3)/e/r);		// true anomaly
    if ( dot(rv,vv,3) < 0 ) {			// fix quadrant
        nu = 2*pi-nu;
    }
#endif

#ifdef USE_SMOOTH_HEAVISIDE
    adouble nu = smooth_heaviside(dot(rv, vv, 3), a_eps) * acos(dot(ev, rv, 3) / e / r)
                 + smooth_heaviside(-dot(rv, vv, 3), a_eps) * (2 * pi - acos(dot(ev, rv, 3) / e / r));
#endif

    oe[0] = a;
    oe[1] = e;
    oe[2] = i;
    oe[3] = Om;
    oe[4] = om;
    oe[5] = nu;

    return;

}


void oe2rv(MatrixXd &oe, double mu, MatrixXd *ri, MatrixXd *vi) {
    double a = oe(0), e = oe(1), i = oe(2), Om = oe(3), om = oe(4), nu = oe(5);
    double p = a * (1 - e * e);
    double r = p / (1 + e * cos(nu));
    MatrixXd rv(3, 1);
    rv(0) = r * cos(nu);
    rv(1) = r * sin(nu);
    rv(2) = 0.0;

    MatrixXd vv(3, 1);

    vv(0) = -sin(nu);
    vv(1) = e + cos(nu);
    vv(2) = 0.0;
    vv *= sqrt(mu / p);

    double cO = cos(Om), sO = sin(Om);
    double co = cos(om), so = sin(om);
    double ci = cos(i), si = sin(i);

    MatrixXd R(3, 3);
    R(0, 0) = cO * co - sO * so * ci;
    R(0, 1) = -cO * so - sO * co * ci;
    R(0, 2) = sO * si;
    R(1, 0) = sO * co + cO * so * ci;
    R(1, 1) = -sO * so + cO * co * ci;
    R(1, 2) = -cO * si;
    R(2, 0) = so * si;
    R(2, 1) = co * si;
    R(2, 2) = ci;

    *ri = R * rv;
    *vi = R * vv;

    return;
}

////////////////////////////////////////////////////////////////////////////
///////////////////////      END OF FILE     ///////////////////////////////
////////////////////////////////////////////////////////////////////////////
